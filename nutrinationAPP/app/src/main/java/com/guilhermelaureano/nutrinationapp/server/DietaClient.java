package com.guilhermelaureano.nutrinationapp.server;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.widget.Toast;

import com.google.gson.Gson;
import com.guilhermelaureano.nutrinationapp.Utils.Contants;
import com.guilhermelaureano.nutrinationapp.model.Nutricionista;
import com.guilhermelaureano.nutrinationapp.model.Paciente;
import com.guilhermelaureano.nutrinationapp.view.HomeActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class DietaClient {

    private String url = Contants.URL_BASE +"/dieta/paciente/";
    private AsyncHttpClient client;
    private Activity activity;
    private AlertDialog dlgCarregando;
    Gson gson = new Gson();

    public DietaClient(Activity activity) {
        this.activity = activity;
        dlgCarregando = (new AlertDialog.Builder(activity)).create();
        dlgCarregando.setTitle("Aguarde");
        dlgCarregando.setMessage("Requisitando banco de dados...");
        dlgCarregando.setCanceledOnTouchOutside(false);
    }


    public void callDieta(Paciente paciente){
        client = new AsyncHttpClient();
        client.get(activity, url + paciente.getIdUsuario() + "/" + paciente.getIdUsuario() , new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                dlgCarregando.show();
            }

            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                String resJSON = new String(bytes);
                Nutricionista nutricionista = new Nutricionista();
                nutricionista = gson.fromJson(resJSON, Nutricionista.class);
                dlgCarregando.dismiss();
                if(nutricionista!= null) {
                    //Montar a intenção com parametro
                    Intent it = new Intent(activity, HomeActivity.class);
                    it.putExtra("nutricionista", nutricionista);
                    activity.startActivity(it);
                }
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                Toast.makeText(activity, "Erro na requisição", Toast.LENGTH_SHORT).show();
                dlgCarregando.dismiss();
            }
        });

    }
}

