package com.guilhermelaureano.nutrinationapp.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.guilhermelaureano.nutrinationapp.R;
import com.guilhermelaureano.nutrinationapp.control.ContatoControl;
import com.guilhermelaureano.nutrinationapp.model.Nutricionista;

public class ContatoActivity extends AppCompatActivity {
    private Nutricionista nutricionista;
    private ContatoControl contatoControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contato);
        contatoControl = new ContatoControl(this);
    }

    public void voltar(View v){
        this.setResult(this.RESULT_CANCELED);
        this.finish();
    }

    public void ligarTelefone(View v){}

    public void ligarCelular(View v){}

    public void enviarEmail(View v){}

}
