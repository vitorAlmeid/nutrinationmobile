package com.guilhermelaureano.nutrinationapp.Interfaces;

public interface OnTaskCompleted {
    void onTaskCompleted(String response);
}
