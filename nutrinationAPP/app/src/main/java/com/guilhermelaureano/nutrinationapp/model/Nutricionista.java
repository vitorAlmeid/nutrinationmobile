package com.guilhermelaureano.nutrinationapp.model;

import java.io.Serializable;

//transient IgnoreProperties({"bairro", "cep", "cidade", "cpf", "estado", "numero", "permissao", "rua", "senha"})
public class Nutricionista implements Serializable {

    private Long idUsuario;
    private String celular;
    private String crnEstado;
    private String email;
    private String nome;
    private String telefone;
    private transient String bairro;
    private transient String cep;
    private transient String cidade;
    private transient String cpf;
    private transient String estado;
    private transient String numero;
    private transient String premissao;
    private transient String rua;
    private transient String senha;

    public Nutricionista() {
    }

    public Nutricionista(Long idUsuario, String celular, String crnEstado, String email, String nome, String telefone) {
        this.idUsuario = idUsuario;
        this.celular = celular;
        this.crnEstado = crnEstado;
        this.email = email;
        this.nome = nome;
        this.telefone = telefone;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCrnEstado() {
        return crnEstado;
    }

    public void setCrnEstado(String crnEstado) {
        this.crnEstado = crnEstado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Override
    public String toString() {
        return "Nutricionista{" +
                "idUsuario=" + idUsuario +
                ", celular='" + celular + '\'' +
                ", crnEstado='" + crnEstado + '\'' +
                ", email='" + email + '\'' +
                ", nome='" + nome + '\'' +
                ", telefone='" + telefone + '\'' +
                '}';
    }
}
