package com.guilhermelaureano.nutrinationapp.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.guilhermelaureano.nutrinationapp.R;
import com.guilhermelaureano.nutrinationapp.control.ChangePasswordControl;

import java.io.UnsupportedEncodingException;

public class PasswordActivity extends AppCompatActivity {

    ChangePasswordControl changePasswordControl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        changePasswordControl = new ChangePasswordControl(this);
    }

    public void voltar(View v){
        this.setResult(this.RESULT_CANCELED);
        this.finish();
    }

    public void saveNewPassword(View v) throws UnsupportedEncodingException {
        changePasswordControl.changePasswordAction();

    }
    public void cancel(View v){
        changePasswordControl.cancelarAction();
    }
}
