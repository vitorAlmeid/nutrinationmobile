package com.guilhermelaureano.nutrinationapp.dto;

public class BuscaId {

    private Long idUsuario;

    public BuscaId(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }
}
