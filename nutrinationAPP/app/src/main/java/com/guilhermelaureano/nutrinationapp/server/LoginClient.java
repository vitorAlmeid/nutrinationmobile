package com.guilhermelaureano.nutrinationapp.server;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.provider.SyncStateContract;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.guilhermelaureano.nutrinationapp.Utils.Contants;
import com.guilhermelaureano.nutrinationapp.Utils.Utils;
import com.guilhermelaureano.nutrinationapp.dto.LoginDto;
import com.guilhermelaureano.nutrinationapp.dto.LoginResponse;
import com.guilhermelaureano.nutrinationapp.model.Nutricionista;
import com.guilhermelaureano.nutrinationapp.view.HomeActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

public class LoginClient {

    private String url = Contants.URL_BASE +"/usuario/login";
    private AsyncHttpClient client;
    private Activity activity;
    private AlertDialog dlgCarregando;
    Gson gson = new Gson();
    final LoginResponse[] response = {new LoginResponse()};

    public LoginClient(Activity activity) {
        this.activity = activity;
        dlgCarregando = (new AlertDialog.Builder(activity)).create();
        dlgCarregando.setTitle("Aguarde");
        dlgCarregando.setMessage("Requisitando banco de dados...");
        dlgCarregando.setCanceledOnTouchOutside(false);
    }

    public void login(LoginDto loginDto) throws UnsupportedEncodingException, InterruptedException {
        client = new AsyncHttpClient();
        StringEntity entity = new StringEntity(gson.toJson(loginDto));
        entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

        client.post(activity, url, entity, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                try {
                    Utils.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                dlgCarregando.show();
            }

            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                String resJSON = new String(bytes);
                response[0] = gson.fromJson(resJSON, LoginResponse.class);
                dlgCarregando.dismiss();
                if(response[0].getUsuario()!= null){
                //Montar a intenção com parametro
                Intent it = new Intent(activity, HomeActivity.class);
                it.putExtra("loginresponse", response[0]);
                activity.startActivity(it);
                } else {
                    AlertDialog notSuccess = (new AlertDialog.Builder(activity)).create();

                    notSuccess.setTitle("Login inválido");
                    notSuccess.setMessage("Email ou senha errados.");
                    notSuccess.setCanceledOnTouchOutside(true);
                    notSuccess.show();
                }

            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                Toast.makeText(activity, "Erro na requisição", Toast.LENGTH_SHORT).show();
                dlgCarregando.dismiss();
            }
        });
}}

