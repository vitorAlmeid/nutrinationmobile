package com.guilhermelaureano.nutrinationapp.control;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.util.Patterns;
import android.widget.EditText;

import com.guilhermelaureano.nutrinationapp.R;
import com.guilhermelaureano.nutrinationapp.dto.LoginDto;
import com.guilhermelaureano.nutrinationapp.dto.LoginResponse;
import com.guilhermelaureano.nutrinationapp.model.Nutricionista;

import com.guilhermelaureano.nutrinationapp.server.LoginClient;
import com.guilhermelaureano.nutrinationapp.view.HomeActivity;

import java.io.UnsupportedEncodingException;


public class LoginControl {

    private Activity activity;
    private EditText editTextEmail, editTextSenha;
    private LoginClient loginClient;

    public LoginControl(Activity activity){
        this.activity = activity;
        loginClient = new LoginClient(activity);
        initComponents();
    }

    private void initComponents(){
        editTextEmail = activity.findViewById(R.id.login_email);
        editTextSenha = activity.findViewById(R.id.login_senha);
    }

    public void userLogin() throws UnsupportedEncodingException, InterruptedException {

        String email = editTextEmail.getText().toString().trim();
        String senha = editTextSenha.getText().toString().trim();

        if (email.isEmpty()) {
            editTextEmail.setError("E-mail é obrigatório!");
            editTextEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editTextEmail.setError("Entre com um e-mail válido!");
            editTextEmail.requestFocus();
            return;
        }

        if (senha.isEmpty()) {
            editTextSenha.setError("Senha é obrigatório!");
            editTextSenha.requestFocus();
            return;
        }

        if (senha.length() < 4) {
            editTextSenha.setError("Senha deve ter no mínimo 4 caracteres.");
            editTextSenha.requestFocus();
            return;
        }
        LoginDto loginDto = new LoginDto(email, senha);
        loginClient.login(loginDto);

        }
}
