package com.guilhermelaureano.nutrinationapp.model;

import java.util.List;

public class Dieta {
    private Long idDieta;
    private String nome;
    private Long idNutricionista;
    private Long idPaciente;
    private List<Refeicao> refeicoes;

    public Dieta() {
    }

    public Long getId() {
        return idDieta;
    }

    public void setId(Long id) {
        this.idDieta = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getIdNutricionista() {
        return idNutricionista;
    }

    public void setIdNutricionista(Long idNutricionista) {
        this.idNutricionista = idNutricionista;
    }

    public Long getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Long idPaciente) {
        this.idPaciente = idPaciente;
    }

    public List<Refeicao> getRefeicoes() {
        return refeicoes;
    }

    public void setRefeicoes(List<Refeicao> refeicoes) {
        this.refeicoes = refeicoes;
    }
}
