package com.guilhermelaureano.nutrinationapp.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.guilhermelaureano.nutrinationapp.R;
import com.guilhermelaureano.nutrinationapp.control.DadosPacienteControl;

import java.io.UnsupportedEncodingException;

public class DadosPacienteActivity extends AppCompatActivity {

    private DadosPacienteControl dadosPacienteControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dados_paciente);
        dadosPacienteControl = new DadosPacienteControl(this);
    }

    public void voltar(View v){
        dadosPacienteControl.voltar();
    }

    public void update(View v) throws UnsupportedEncodingException {
        dadosPacienteControl.update();
    }
}
