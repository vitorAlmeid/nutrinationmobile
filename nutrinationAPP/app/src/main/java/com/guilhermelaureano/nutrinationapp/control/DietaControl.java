package com.guilhermelaureano.nutrinationapp.control;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.guilhermelaureano.nutrinationapp.R;
import com.guilhermelaureano.nutrinationapp.model.Refeicao;

import java.util.ArrayList;

public class DietaControl {
    private Activity activity;
    private Refeicao refeicao;
    private Spinner spRefeicoes;
    private TextView tvRefeicao;
    private ArrayAdapter<Refeicao> adapterRefeicao;

    public DietaControl (Activity activity){
        this.activity = activity;
        spRefeicoes = activity.findViewById(R.id.spRefeicoes);
        tvRefeicao = activity.findViewById(R.id.tvRefeicao);
        initComponents();
    }

    public void initComponents(){
        spRefeicoes = activity.findViewById(R.id.spRefeicoes);
        tvRefeicao = activity.findViewById(R.id.tvRefeicao);
        configurarSpinner();
    }

    public void configurarSpinner() {

        adapterRefeicao = new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, new ArrayList<Refeicao>());
        //setRefeicoes(adapterRefeicao, spRefeicoes);
    }

    public void voltar(){
        activity.setResult(activity.RESULT_CANCELED);
        activity.finish();
    }
}
