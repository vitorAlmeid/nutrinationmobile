package com.guilhermelaureano.nutrinationapp.server;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.widget.Toast;

import com.google.gson.Gson;
import com.guilhermelaureano.nutrinationapp.Utils.Contants;
import com.guilhermelaureano.nutrinationapp.Utils.Utils;
import com.guilhermelaureano.nutrinationapp.control.DadosPacienteControl;
import com.guilhermelaureano.nutrinationapp.dto.BuscaId;
import com.guilhermelaureano.nutrinationapp.dto.LoginDto;
import com.guilhermelaureano.nutrinationapp.dto.LoginResponse;
import com.guilhermelaureano.nutrinationapp.model.Nutricionista;
import com.guilhermelaureano.nutrinationapp.model.Paciente;
import com.guilhermelaureano.nutrinationapp.view.ContatoActivity;
import com.guilhermelaureano.nutrinationapp.view.HomeActivity;
import com.guilhermelaureano.nutrinationapp.view.PasswordActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

public class DadosPacienteClient {

        private String url = Contants.URL_BASE + "/usuario/";
        private AsyncHttpClient client;
        private Activity activity;
        private AlertDialog dlgCarregando;
        Gson gson = new Gson();


        public DadosPacienteClient(Activity activity){
            this.activity = activity;
            dlgCarregando = (new AlertDialog.Builder(activity)).create();
            dlgCarregando.setTitle("Aguarde");
            dlgCarregando.setMessage("Requisitando banco de dados...");
            dlgCarregando.setCanceledOnTouchOutside(false);
        }


        public void update(final Paciente paciente) throws UnsupportedEncodingException {
            client = new AsyncHttpClient();
            StringEntity entity = new StringEntity(gson.toJson(paciente));
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            client.post(activity, url+"cadastrar/", entity, "application/json", new AsyncHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    dlgCarregando.show();
                }

                @Override
                public void onSuccess(int i, Header[] headers, byte[] bytes) {
                    String resJSON = new String(bytes);
                    dlgCarregando.dismiss();
                }

                @Override
                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                    Toast.makeText(activity, "Erro na requisição", Toast.LENGTH_SHORT).show();
                    dlgCarregando.dismiss();
                }
            });

        }


    public void dadosNutricionista(final Long idUsuario) throws UnsupportedEncodingException {
        client = new AsyncHttpClient();
        client.get(activity, url + idUsuario , new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                dlgCarregando.show();
            }

            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                String resJSON = new String(bytes);
                Nutricionista nutricionista = new Nutricionista();
                nutricionista = gson.fromJson(resJSON, Nutricionista.class);
                dlgCarregando.dismiss();
                if(nutricionista!= null) {
                    //Montar a intenção com parametro
                    Intent it = new Intent(activity, ContatoActivity.class);
                    it.putExtra("nutricionista", nutricionista);
                    activity.startActivity(it);
                }
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                Toast.makeText(activity, "Erro na requisição", Toast.LENGTH_SHORT).show();
                dlgCarregando.dismiss();
            }
        });

    }
    }

