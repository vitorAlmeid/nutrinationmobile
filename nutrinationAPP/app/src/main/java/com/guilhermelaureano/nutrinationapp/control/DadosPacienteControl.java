package com.guilhermelaureano.nutrinationapp.control;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.guilhermelaureano.nutrinationapp.R;
import com.guilhermelaureano.nutrinationapp.model.Paciente;
import com.guilhermelaureano.nutrinationapp.server.DadosPacienteClient;

import java.io.UnsupportedEncodingException;

public class DadosPacienteControl {

    DadosPacienteClient dadosPacienteClient;
    private Activity activity;
    private Paciente paciente;
    private EditText editNome;
    private EditText editEmail;
    private EditText editTelefone;
    private EditText editCelular;
    private EditText editLogradouro;
    private EditText editNumero;
    private EditText editCep;
    private EditText editBairro;
    private EditText editCidade;
    private EditText editEstado;

    public DadosPacienteControl(Activity activity){
        this.activity = activity;
        resgateParametro();
        initComponents();
        dadosPacienteClient = new DadosPacienteClient(activity);
    }

    private void initComponents() {
        editNome = activity.findViewById(R.id.editNome);
        editEmail = activity.findViewById(R.id.editEmail);
        editTelefone = activity.findViewById(R.id.editTelefone);
        editCelular = activity.findViewById(R.id.editCelular);
        editLogradouro = activity.findViewById(R.id.editLogradouro);
        editNumero = activity.findViewById(R.id.editNumero);
        editCep = activity.findViewById(R.id.editCep);
        editBairro = activity.findViewById(R.id.editBairro);
        editCidade = activity.findViewById(R.id.editCidade);
        editEstado = activity.findViewById(R.id.editEstado);
        configuraDadosTela();

    }

    private void resgateParametro() {
        paciente = (Paciente) activity.getIntent().getSerializableExtra("paciente");
    }

    private void configuraDadosTela() {
        editNome.setText(paciente.getNome());
        editEmail.setText(paciente.getEmail());
        editTelefone.setText(paciente.getTelefone());
        editCelular.setText(paciente.getCelular());
        editLogradouro.setText(paciente.getRua());
        editNumero.setText(paciente.getNumero());
        editCep.setText(paciente.getCep());
        editBairro.setText(paciente.getBairro());
        editCidade.setText(paciente.getCidade());
        editEstado.setText(paciente.getEstado());
    }

    public void update() throws UnsupportedEncodingException {
        validaCampos();
        dadosPacienteClient.update(paciente);
        Toast.makeText(activity, "Dados salvos", Toast.LENGTH_SHORT).show();
    }

    public void voltar(){
        Intent resultIntent = new Intent();
        resultIntent.putExtra("paciente", paciente);
        activity.setResult(Activity.RESULT_OK, resultIntent);
        activity.finish();
    }

    private void validaCampos(){
        if(!paciente.getNome().equals(editNome.getText().toString())){
            paciente.setNome(editNome.getText().toString());
        }
        if(!paciente.getEmail().equals(editEmail.getText().toString())){
            paciente.setEmail(editEmail.getText().toString());
        }
        if(!paciente.getTelefone().equals(editTelefone.getText().toString())){
            paciente.setTelefone(editTelefone.getText().toString());
        }
        if(!paciente.getCelular().equals(editCelular.getText().toString())){
            paciente.setCelular(editCelular.getText().toString());
        }
        if(!paciente.getCep().equals(editCep.getText().toString())){
            paciente.setCep(editCep.getText().toString());
        }
        if(!paciente.getNumero().equals(editNumero.getText().toString())){
            paciente.setNumero(editNumero.getText().toString());
        }
        if(!paciente.getRua().equals(editLogradouro.getText().toString())){
            paciente.setRua(editLogradouro.getText().toString());
        }
        if(!paciente.getCidade().equals(editCidade.getText().toString())){
            paciente.setCidade(editCidade.getText().toString());
        }
        if(!paciente.getEstado().equals(editEstado.getText().toString())){
            paciente.setEstado(editEstado.getText().toString());
        }
    }
}
