package com.guilhermelaureano.nutrinationapp.control;

import android.app.Activity;
import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;

import com.guilhermelaureano.nutrinationapp.R;
import com.guilhermelaureano.nutrinationapp.model.Paciente;
import com.guilhermelaureano.nutrinationapp.server.DadosPacienteClient;

import java.io.UnsupportedEncodingException;

public class ChangePasswordControl {
    
    private Activity activity;
    private EditText editOldPassword;
    private EditText editNewPassword1;
    private EditText editNewPassword2;
    private Paciente paciente;
    DadosPacienteClient dadosPacienteClient;

    
    public ChangePasswordControl(Activity activity){
        this.activity = activity;
        dadosPacienteClient = new DadosPacienteClient(activity);
        resgateParametro();
        initComponents();
    }

    public void changePasswordAction() throws UnsupportedEncodingException {
        String newPassword1 = editNewPassword1.getText().toString();
        String newPassword2 = editNewPassword2.getText().toString();
        String oldPassword = editOldPassword.getText().toString();

        if (validateFields(newPassword1, newPassword2, oldPassword)){
            paciente.setSenha(newPassword1);
            dadosPacienteClient.update(paciente);
            Toast.makeText(activity, "Nova senha sal va!", Toast.LENGTH_SHORT).show();
        }

    }

    public void cancelarAction(){
        Intent resultIntent = new Intent();
        resultIntent.putExtra("paciente", paciente);
        activity.setResult(Activity.RESULT_OK, resultIntent);
        activity.finish();
    }

    private void resgateParametro() {
        paciente = (Paciente) activity.getIntent().getSerializableExtra("paciente");
    }

    private void initComponents(){
        editOldPassword = activity.findViewById(R.id.editOldPassword);
        editNewPassword1 = activity.findViewById(R.id.editNewPassword1);
        editNewPassword2 = activity.findViewById(R.id.editNewPassword2);

    }

    private boolean validateFields(String newPassword1, String newPassword2, String oldPassword){
        if (newPassword1.isEmpty()) {
            editNewPassword1.setError("Campo obrigatório!");
            editNewPassword1.requestFocus();
            return false;
        }

        if (newPassword2.isEmpty()) {
            editNewPassword2.setError("Campo obrigatório!");
            editNewPassword2.requestFocus();
            return false;
        }

        if (oldPassword.isEmpty()) {
            editOldPassword.setError("Campo obrigatório!");
            editOldPassword.requestFocus();
            return false;
        }

        if (newPassword1.length() < 4 || newPassword2.length() < 4) {
            editNewPassword1.setError("Senha deve ter no mínimo 4 caracteres.");
            editNewPassword2.setError("Senha deve ter no mínimo 4 caracteres.");
            return false;
        }

        if (!oldPassword.equals(paciente.getSenha())){
            editOldPassword.setError("Senha antiga errada!");
            editOldPassword.requestFocus();
            return false;
        }

        if (!newPassword1.equals(newPassword2)){
            editNewPassword1.setError("As novas senhas precisam ser iguais");
            editNewPassword2.setError("As novas senhas precisam ser iguais");
            return false;
        }

        return true;
    }
}
