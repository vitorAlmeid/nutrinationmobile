package com.guilhermelaureano.nutrinationapp.dto;

import com.guilhermelaureano.nutrinationapp.model.Paciente;

import java.io.Serializable;

public class LoginResponse implements Serializable {

    private boolean error;
    private String message;
    private Paciente usuario;

    public LoginResponse(boolean error, String message, Paciente usuario) {
        this.error = error;
        this.message = message;
        this.usuario = usuario;
    }

    public LoginResponse() {

    }

    public boolean isError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public Paciente getUsuario() {
        return usuario;
    }
}