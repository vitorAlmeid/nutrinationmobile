package com.guilhermelaureano.nutrinationapp.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.guilhermelaureano.nutrinationapp.Interfaces.OnTaskCompleted;
import com.guilhermelaureano.nutrinationapp.R;
import com.guilhermelaureano.nutrinationapp.control.LoginControl;

import java.io.UnsupportedEncodingException;

public class LoginActivity extends AppCompatActivity{
    private LoginControl control;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        control = new LoginControl(this);
    }

    public void entrar(View v) throws UnsupportedEncodingException, InterruptedException {
        control.userLogin();
    }
}
