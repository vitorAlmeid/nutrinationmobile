package com.guilhermelaureano.nutrinationapp.model;

import java.io.Serializable;

//transient  IgnoreProperties({"crnEstado", "permissao", "senha"})
public class Paciente  implements Serializable {
    private Long idUsuario;
    private String bairro;
    private String celular;
    private String cep;
    private String cidade;
    private String cpf;
    private String email;
    private String estado;
    private String nome;
    private String numero;
    private String rua;
    private String telefone;
    private String senha;
    private String crnEstado;
    private String permissao;
    private Long child;


    public Paciente() {
    }

    public Paciente(Long idUsuario, String bairro, String celular, String cep, String cidade, String cpf, String email, String estado, String nome, String numero, String rua, String telefone, Long child) {
        this.idUsuario = idUsuario;
        this.bairro = bairro;
        this.celular = celular;
        this.cep = cep;
        this.cidade = cidade;
        this.cpf = cpf;
        this.email = email;
        this.estado = estado;
        this.nome = nome;
        this.numero = numero;
        this.rua = rua;
        this.telefone = telefone;
        this.child = child;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getCrnEstado() {
        return crnEstado;
    }

    public void setCrnEstado(String crnEstado) {
        this.crnEstado = crnEstado;
    }

    public String getPermissao() {
        return permissao;
    }

    public void setPermissao(String permissao) {
        this.permissao = permissao;
    }

    public Long getChild() {
        return child;
    }

    public void setChild(Long child) {
        this.child = child;
    }

    @Override
    public String toString() {
        return "Paciente{" +
                "idUsuario=" + idUsuario +
                ", bairro='" + bairro + '\'' +
                ", celular='" + celular + '\'' +
                ", cep='" + cep + '\'' +
                ", cidade='" + cidade + '\'' +
                ", cpf='" + cpf + '\'' +
                ", email='" + email + '\'' +
                ", estado='" + estado + '\'' +
                ", nome='" + nome + '\'' +
                ", numero='" + numero + '\'' +
                ", rua='" + rua + '\'' +
                ", telefone='" + telefone + '\'' +
                '}';
    }
}
