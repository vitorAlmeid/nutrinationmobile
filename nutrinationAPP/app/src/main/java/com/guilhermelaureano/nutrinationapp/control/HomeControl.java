package com.guilhermelaureano.nutrinationapp.control;

import android.app.Activity;
import android.content.Intent;

import com.guilhermelaureano.nutrinationapp.dto.LoginResponse;
import com.guilhermelaureano.nutrinationapp.model.Paciente;
import com.guilhermelaureano.nutrinationapp.server.DadosPacienteClient;
import com.guilhermelaureano.nutrinationapp.server.LoginClient;
import com.guilhermelaureano.nutrinationapp.view.DadosPacienteActivity;
import com.guilhermelaureano.nutrinationapp.view.DietaActivity;
import com.guilhermelaureano.nutrinationapp.view.HomeActivity;
import com.guilhermelaureano.nutrinationapp.view.PasswordActivity;

import java.io.UnsupportedEncodingException;

public class HomeControl {

    private Activity activity;
    private LoginResponse loginResponse;
    private Paciente paciente;
    private DadosPacienteClient dadosPacienteClient;


    public HomeControl(Activity activity){
        this.activity = activity;
        dadosPacienteClient = new DadosPacienteClient(activity);
        resgateParametro();
    }

    private void resgateParametro() {
        loginResponse = (LoginResponse) activity.getIntent().getSerializableExtra("loginresponse");
        paciente = new Paciente();
        paciente = loginResponse.getUsuario();
    }

    public void dadosPacienteAction() {

        //Montar a intenção com parametro
        Intent it = new Intent(activity, DadosPacienteActivity.class);
        it.putExtra("paciente", paciente);
        activity.startActivityForResult(it, 355);
    }

    public void changePassword(){
        //Montar a intenção com parametro
        Intent it = new Intent(activity, PasswordActivity.class);
        it.putExtra("paciente", paciente);
        activity.startActivityForResult(it, 455);
    }

    public void dadosNutricionistaAction() throws UnsupportedEncodingException {
        dadosPacienteClient.dadosNutricionista(paciente.getChild());
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == activity.RESULT_OK){
            paciente = (Paciente) data.getSerializableExtra("paciente");
        }
    }

    public void dietaAction() {
        Intent it = new Intent(activity, DietaActivity.class);
        it.putExtra("paciente", paciente);
        activity.startActivity(it);
    }
}
