package com.guilhermelaureano.nutrinationapp.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.guilhermelaureano.nutrinationapp.R;
import com.guilhermelaureano.nutrinationapp.control.HomeControl;
import com.guilhermelaureano.nutrinationapp.model.Paciente;

import java.io.UnsupportedEncodingException;

public class HomeActivity extends AppCompatActivity {

    HomeControl homeControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        homeControl = new HomeControl(this);
    }


    public void dadosPaciente(View v){
        homeControl.dadosPacienteAction();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        homeControl.onActivityResult(requestCode, resultCode, data);
    }

    public void changePassword(View v){
        homeControl.changePassword();
    }

    public void contato(View v) throws UnsupportedEncodingException {
        homeControl.dadosNutricionistaAction();
    }

    public void dieta(View v){
        homeControl.dietaAction();
    }
}

