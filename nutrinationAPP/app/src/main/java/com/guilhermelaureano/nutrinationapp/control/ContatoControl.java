package com.guilhermelaureano.nutrinationapp.control;

import android.app.Activity;
import android.widget.TextView;

import com.guilhermelaureano.nutrinationapp.R;
import com.guilhermelaureano.nutrinationapp.model.Nutricionista;
import com.guilhermelaureano.nutrinationapp.server.DadosPacienteClient;

public class ContatoControl {
    private Activity activity;
    private TextView tvNome;
    private TextView tvCrn;
    private TextView tvTelefone;
    private TextView tvCelular;
    private TextView tvEmail;
    private DadosPacienteClient dadosPacienteClient;
    private Nutricionista nutricionista;

    public ContatoControl(Activity activity){
        this.activity = activity;
        dadosPacienteClient = new DadosPacienteClient(activity);
        resgateParametro();
        initComponents();
        populaTela();
    }

    private void resgateParametro() {
        nutricionista = (Nutricionista) activity.getIntent().getSerializableExtra("nutricionista");
    }

    private void initComponents() {
        tvNome = activity.findViewById(R.id.tvNutri_nome);
        tvCrn = activity.findViewById(R.id.tvNutri_crn);
        tvTelefone = activity.findViewById(R.id.tvNutri_telefone);
        tvCelular = activity.findViewById(R.id.tvNutri_celular);
        tvEmail = activity.findViewById(R.id.tvNutri_email);
    }

    private void populaTela(){
        tvNome.setText(nutricionista.getNome());
        tvCrn.setText("CRN: " +nutricionista.getCrnEstado());
        tvTelefone.setText(nutricionista.getTelefone());
        tvCelular.setText(nutricionista.getCelular());
        tvEmail.setText(nutricionista.getEmail());
    }

}
