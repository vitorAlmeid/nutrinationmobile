package com.guilhermelaureano.nutrinationapp.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.guilhermelaureano.nutrinationapp.R;
import com.guilhermelaureano.nutrinationapp.control.DietaControl;

public class DietaActivity extends AppCompatActivity {
    private DietaControl control;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dieta);
        control = new DietaControl(this);
    }

    public void voltar(View v){control.voltar();}


}
